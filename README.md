# GitLab Chart for Google GKE Marketplace

## For Developers

Developers should consult
[the instructions for building and testing](doc/building-deployer.md) for
more information about how to use the included build tooling.

## For Users and Administrators

Consult the [documentation about installing GitLab from the Marketplace](doc/installing.md).
