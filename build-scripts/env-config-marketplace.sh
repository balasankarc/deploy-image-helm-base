#!/usr/bin/env bash

# Make sure certain sections of the environment contain the proper items
# for building marketplace
# Defaults to dry run; add --commit to actually apply to the environment

task_list=""

merge_task()
{
  task="$1"

  echo -en "${task_list}\n${task}"
}

add_task() {
    task="$1"

    if [ -z "${task_list}" ]; then
        task_list="$(echo -e "${task}")"
    else
        task_list=$(merge_task "${task}")
    fi
}

# extracted to function because trying to run it as a heredoc
# variable was terrible
install_mpdev() {
    BIN_FILE="$HOME/bin/mpdev"
    docker run \
        gcr.io/cloud-marketplace-tools/k8s/dev \
        cat /scripts/dev > "$BIN_FILE"
    chmod +x "$BIN_FILE"
}

is_dry_run="true"

if [ "$1" = "--commit" ]; then
    is_dry_run="false"
fi

if kubectl get crd --all-namespaces|grep -q 'kapplications.app.k8s.io' > /dev/null 2>&1; then
    echo "The Marketplace Application Custom Resource Definition present"
else
    add_task "kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"
fi

add_task "docker pull gcr.io/cloud-marketplace-tools/k8s/dev"
add_task "docker pull gcr.io/cloud-marketplace-tools/k8s/deployer_helm_tiller/onbuild:latest"
add_task "install_mpdev"

echo "Starting Run..."
printf '=%.0s' {1..80}
echo ""
while IFS= read -r task
do
    if [ "${is_dry_run}" = "false" ]; then
        ${task}
    else
        echo -e "${task}"
    fi
done < <(printf '%s\n' "${task_list}")

if [ "${is_dry_run}" = "true" ]; then
  printf '=%.0s' {1..80}
  echo ""
  echo -e "NOTE: Run with --commit to apply changes"
fi
