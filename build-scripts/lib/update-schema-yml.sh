#!/usr/bin/env bash
# update-schema-yml
# Script to update the image tags of schema.yml based on the image tags
# of the rendered helm template.

set -e

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

export GL_MP_HELM_RBAC_TEMPLATE="${GL_MP_SCRATCH}/helm_template_with_rbac.yaml"
export GL_MP_SCHEMA_HEAD="${GL_MP_SCRATCH}/.schema.head.yaml"
export GL_MP_SCHEMA_RBAC_ENTRIES="${GL_MP_SCRATCH}/.rbac_entries.yaml"
export GL_MP_SCHEMA_TAIL="${GL_MP_SCRATCH}/.schema.tail.yaml"

function reset_schema_yaml() {
    [ -f "${GL_MP_SCHEMA_HEAD}" ] && rm "${GL_MP_SCHEMA_HEAD}"
    [ -f "${GL_MP_SCHEMA_RBAC_ENTRIES}" ] && rm "${GL_MP_SCHEMA_RBAC_ENTRIES}"
    [ -f "${GL_MP_SCHEMA_TAIL}" ] && rm "${GL_MP_SCHEMA_TAIL}"
    return 0
}

function findImage() {
    grep -q "default: [\"]\{0,1\}\$REGISTRY/$1" "${GL_MP_SCHEMA_FILE}"
}

function replaceTagOnImage() {
    IMAGE=$1
    TAG=$2
    pattern="s/[\"]\{0,1\}\$REGISTRY\/${IMAGE}:.*[\"]\{0,1\}$/\"\$REGISTRY\/${IMAGE}:${TAG}\"/"
    add_dead_file "${GL_MP_SCHEMA_FILE}-e"
    sed -i -e "${pattern}" "${GL_MP_SCHEMA_FILE}"
}

if [ ! -f "${GL_MP_SCHEMA_FILE}" ]; then
  display_failure "Unable to find file: ${GL_MP_SCHEMA_FILE}"
fi

display_task "Resetting schema partials in scratch environment"
if [ -e "${GL_MP_SCRATCH}" ]; then
   if [ -d "${GL_MP_SCRATCH}" ]; then
       if reset_schema_yaml; then
           display_success "old schema partials removed"
       else
           display_failure "failed to remove old schema partials"
       fi
   else
       display_failure "${GL_MP_SCRATCH} exists and is not a directory"
   fi
fi

mkdir -p "${GL_MP_SCRATCH}" || display_failure "scratch reset failed"

has_failed_image="no"
for image in $("${GL_MP_LIST_IMAGES}"); do
    sourceImageName="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    sourceImageTag="${image//*:}"
    image_msg="Image: ${sourceImageName}"
    if ! findImage "${sourceImageName}"; then
        image_msg="${image_msg} !! NOT FOUND"
        has_failed_image="yes"
    else
        replaceTagOnImage "${sourceImageName}" "${sourceImageTag}"
    fi
    display_success "${image_msg}"
done

if [ ${has_failed_image} = "yes" ]; then
    display_failure "Failed to locate some images, review logs above"
fi

helm_chart_path=$(get_gitlab_helm_chart_path)
pushd "${helm_chart_path}" > /dev/null
display_task "Generating helm template containing RBAC entries"
if helm template . -f "${GL_MP_VALUES_YAML}" \
    --set certmanager-issuer.email=none@none.com,gitlab.nginx-ingress.serviceAccount.create=true,gitlab.shared-secrets.serviceAccount.create=true \
    --name "${GL_MP_APP_INSTANCE_NAME}" > "${GL_MP_HELM_RBAC_TEMPLATE}" ; then
    display_success "generated yaml from helm template invocation"
else
    display_failure "failed to generate yaml from helm template invocation"
fi
popd > /dev/null

display_task "Exporting RBAC schema entries from Helm Template"
if "${GL_MP_RBAC_EXPORTER}" -t "${GL_MP_HELM_RBAC_TEMPLATE}" -i 2 -m "${GL_MP_SA_MAPPING_YAML}"> "${GL_MP_SCHEMA_RBAC_ENTRIES}"; then
    display_success "generated RBAC schema entries"
else
    display_failure "failed to generate RBAC schema entries"
fi

# TODO: How to wrap this up for a guard?
#       Would prefer to set the variable & verify grep came back with
#       non-zero at same time
#       For now, it will fail if grep fails because of -e
line_number=$(grep -n "\$RBAC_ENTRIES" "${GL_MP_SCHEMA_FILE}"|cut -d ':' -f 1)

display_task "Exporting Schema Head"
if head -n $((line_number-1)) "${GL_MP_SCHEMA_FILE}" > "${GL_MP_SCHEMA_HEAD}"; then
    display_success "succeeded"
else
    display_failure "failed"
fi

display_task "Exporting Schema Tail"
if sed -n $((line_number+1))', $p' "${GL_MP_SCHEMA_FILE}" > "${GL_MP_SCHEMA_TAIL}"; then
    display_success "succeeded"
else
    display_failure "failed"
fi

display_task "Re-assembling Schema with RBAC entries"
if  cp "${GL_MP_SCHEMA_HEAD}" "${GL_MP_SCHEMA_FILE}"; then
    display_success "schema head injected"
else
    display_failure "schema head injection failed"
fi

if cat "${GL_MP_SCHEMA_RBAC_ENTRIES}" >> "${GL_MP_SCHEMA_FILE}"; then
    display_success "schema RBAC entries injected"
else
    display_failure "schema RBAC entries injection failed"
fi

if cat "${GL_MP_SCHEMA_TAIL}" >> "${GL_MP_SCHEMA_FILE}"; then
    display_success "schema tail injected"
else
    display_failure "schema tail injection failed"
fi
