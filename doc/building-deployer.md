# Building the GitLab Deployer Container for Google Marketplace

## Overview

The deployer container is built with [tooling provided by Google](https://github.com/GoogleCloudPlatform/marketplace-k8s-app-tools)
invoked using a series of custom scripts.

## Build Dependencies

Contributors should have the following tools installed:

> **NOTE**
> `env-doctor.sh` and `env-config-marketplace.sh` are provided to assist
> with environment setup. The doctor checks for the requirements below.
> After following the steps to create a cluster use `env-config-marketplace`
> to automatically install the Google Marketplace Tools.

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [gCloud](https://cloud.google.com/sdk/gcloud/)
- [Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [Helm](https://github.com/kubernetes/helm/blob/master/docs/install.md)
- Container Runtime
  - [Podman](https://podman.io)
  - [Docker](https://www.docker.com)
- [Google Marketplace Tools](https://github.com/GoogleCloudPlatform/marketplace-k8s-app-tools/blob/master/docs/tool-prerequisites.md)
  - Make sure `mpdev` is available in the path
  - [Make sure to apply the Application CRD to the cluster](https://github.com/GoogleCloudPlatform/marketplace-k8s-app-tools/blob/master/docs/tool-prerequisites.md#install-application-crd)
- Ruby version 2.3 or later

## Setting up the Build Environment

### Create a Google Kubernetes Engine Cluster

Create a new cluster using the gcloud tool and add connection information to
the kubectl command.

```shell
export GL_MP_CLUSTER=a-cluster-name
export GL_MP_ZONE=a-gke-zone
export GL_MP_PROJECT_ID=a-project-id

gcloud container clusters create "$GL_MP_CLUSTER" --zone "$GL_MP_ZONE" --project "$GL_MP_PROJECT_ID"
gcloud container clusters get-credentials "$GL_MP_CLUSTER" --zone "$GL_MP_ZONE" --project "$GL_MP_PROJECT_ID"
```

### Configuring Authentication

The build and deployment process depends on connections to the container
registry where deployer images live. Configure the container runtime to
authenticate using gcloud credentials.

```shell
gcloud auth configure-docker
```

### Environment Variables

The build process is controlled by several environment variables.

#### Explanation of Variables

The following variables are required to build the deployer.

- ***GITLAB\_BRANCH\_NAME*** defines a branch in Cloud Native GitLab upstream
  repository to use for building a deployer container. If this is set it
  takes precedence over all other possible settings.
- ***GL\_CHART\_VERSION*** defines the version number of a Cloud Native
  GitLab helm chart.
  Cloud Native GitLab that will be packaged for the Google GKE Marketplace.
- ***GL\_MP\_REGISTRY*** defines the URL to the container registry where
  completed deployer and supporting images will be pushed.
- ***GL\_MP\_APP\_NAME*** defines the application name when deployed into
  the kubernetes cluster. It is appeneded to ***GL\_MP\_REGISTRY*** when
  pushing the deployer and supporting containers.

> **NOTE**
> If neither ***GITLAB\_BRANCH\_NAME*** nor ***GL\_CHART\_VERSION*** are set
> in the environment the builder defaults to building from the GitLab Cloud
> Native master branch.

Setting ***GL\_MP\_DEBUG*** to `yes` will enable much more verbose logging
when attempting to troubleshoot problems during builds.

The following variables are required in addition to the above when testing the deployer image.

- ***GL\_MP\_INSTANCE\_NAME*** defines the ***RELEASE\_NAME*** within Kubernetes
  for the deployer image during build time.
- ***GL\_MP\_NAMESPACE*** defines the namespace a test deployment will
  occupy within Kubernetes.
- ***GL\_MP\_TLD*** defines the top level domain a GitLab Marketplace
  deployment will occupy.  eg: gitlab.TLD


#### Configuring for Local Builds

Copy `conf/user_env.EXAMPLE`, uncomment and modify the values. The build
scripts will source the file if it has been created.

```sh
cp conf/user_env.EXAMPLE conf/user_env
```

#### Configuring for Continuous Integration Builds
Define the values from `conf/user_env.EXAMPLE` in the continuous integration
environment to enable builds while hiding specific endpoints.

## Building the Deployer

The following commands:

- build a deployer container image
- push the resulting image to the marketplace registry
- mirror the updated images into the marketplace registry
- run the deployer on a test cluster installing a GitLab instance
- tear down the testing instance

```sh
./build-scripts/build-marketplace-release.sh
./build-scripts/push-marketplace-release.sh
./build-scripts/push-container-dependencies.sh
```

## Testing the Deployer

The deployer image will create self-signed certificates but this can be
problematic in modern browsers. [The section on installing the marketplace
image shows how to set the TLS secret with custom
certificates](doc/installing.md#command-line-instructions).

Test the deployer using after setting the TLS secret using:

```sh
./build-scripts/test-marketplace-release.sh
```

Tear down the test deployment using:

```sh
./build-scripts/tear-down-marketplace-release-test.sh
```

## Common Issues

- `error: unable to recognize "STDIN": no matches for kind "Application" in version "app.k8s.io/v1beta1"`
  - The Application CRD from Google Marketplace Tools has not been installed
